# README #


### Introduction ###

The purpose of this project is to display mastery of python's Pillow image processing library, as well as mastery of the open source development model. This project allows a user to load an image and manipulate the image using several built in functions. These functions can display two images masked onto one another in concentric rings, split an image into equal parts and shuffle these parts around, invert the colors in the image and display the image in a particular aspect ratio.

### Requirements ###

This program requires the following Library:

* Pillow (https://pypi.org/project/Pillow/)

### Installation ###

* Clone from https://bitbucket.org/JBressett/finalproject/src/master/
* Run gui.py from the command line

### Contributors ###

* Jack Bressett 
* Sabrina Honigman
* Liam McCormick
* Armani Rogers
* Ben Soderberg